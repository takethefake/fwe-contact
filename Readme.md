# Fwe Contact application

## Installation

rename environment files from `.env.example` to `.env` in `./` and `./packages/server`

```
docker-compose run --rm server bash
yarn install
yarn bootstrap
```

this should install all dependencies and

afterwards we want to setup our databases while being still in the server container

```
cd packages/server
yarn typeorm:sync
yarn fixtures
```

afterwards we exit the container and start the whole application

```
docker-compose run -d
```

logs and errors can be seen with

```
docker-compose logs -f
```

## Tests

To run the test visit `packages/client` and start them with `yarn test`

