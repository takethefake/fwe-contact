export interface User {
  id: string;
  email: string;
  website: string;
  company: string;
  phone: string;
  name: string;
  userName: string;
}

export interface CreateUserInput {
  email: string;
  website: string;
  company: string;
  phone: string;
  name: string;
  userName: string;
}
