import React from "react";

import { Layout as AntLayout } from "antd";
import { Header } from "./Header";
import { useWindowSize, isOnMobile } from "../hooks/useWindowSize";

const { Content, Footer } = AntLayout;

export const Layout: React.FC = ({ children }) => {
  const windowSize = useWindowSize();
  const isMobile = isOnMobile(windowSize);
  return (
    <AntLayout className="layout" style={{ height: "100%" }}>
      <AntLayout>
        <Header />
        <Content style={{ padding: isMobile ? "10px" : "25px 50px" }}>
          <div
            style={{
              background: "#fff",
              padding: isMobile ? 12 : 24,
              height: "100%"
            }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Fwe Contacts ©2020 Created by Daniel Schulz
        </Footer>
      </AntLayout>
    </AntLayout>
  );
};
