import React from "react";
import fweLogo from "../assets/hda_logo.png";

import { Layout, Menu } from "antd";

const { Header: AntHeader } = Layout;

export const Header: React.FC = () => {
  return (
    <AntHeader>
      <div style={{ height: "100%", display: "flex", alignItems: "center" }}>
        <img src={fweLogo} style={{ height: "70%" }} alt="fwe logo" />
        <span
          style={{
            color: "#ffffff",
            fontSize: "16px",
            marginTop: "14px",
            marginLeft: "5px"
          }}
        >
          Contacts
        </span>
      </div>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["2"]}
        style={{ lineHeight: "64px" }}
      ></Menu>
    </AntHeader>
  );
};
