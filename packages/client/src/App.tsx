import React from "react";
import { Layout } from "./components/Layout";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { DashboardPage } from "./pages/Dashboard/DashboardPage";
import { UserProvider } from "./context/UserContext";

const App = () => {
  return (
    <Router>
      <UserProvider>
        <Layout>
          <Switch>
            <Route exact path="/" component={DashboardPage} />
          </Switch>
        </Layout>
      </UserProvider>
    </Router>
  );
};

export default App;
