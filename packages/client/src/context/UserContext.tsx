import React, { useState, useEffect, useContext } from "react";
import { User, CreateUserInput } from "../interfaces/UserInterface";

interface UserContext {
  state: {
    users: User[];
    selectedUser: User | null;
    loading: boolean;
  };
  actions: {
    fetchUsers: () => void;
    addUser: (userToCreate: CreateUserInput) => Promise<void>;
    removeUser: (user: User) => void;
    selectUser: (user: User | null) => void;
  };
}

export const initialState = {
  state: { users: [], loading: false, selectedUser: null },
  actions: {
    fetchUsers: () => {},
    addUser: () => Promise.resolve(),
    removeUser: () => {},
    selectUser: () => {}
  }
};

export const userContext = React.createContext<UserContext>(initialState);

export const UserProvider: React.FC = ({ children }) => {
  const localStorageUser = window.localStorage.getItem("selectedUser");
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState(false);
  const [selectedUser, setSelectedUser] = useState<User | null>(
    localStorageUser ? JSON.parse(localStorageUser) : null
  );

  useEffect(() => {
    (async () => {
      await fetchUsers();
    })();
  }, []);

  const fetchUsers = async () => {
    setLoading(true);
    const data = (await fetch("/users").then(r => r.json())) as User[];
    setUsers(data);
    setLoading(false);
  };

  const selectUser = (user: User | null) => {
    window.localStorage.setItem("selectedUser", JSON.stringify(user));
    setSelectedUser(user);
  };

  const addUser = async (values: CreateUserInput) => {
    const newUser = await fetch("/users", {
      method: "POST",
      body: JSON.stringify(values),
      headers: {
        "content-type": "application/json"
      }
    }).then(r => r.json());
    setUsers([...users, newUser]);
  };

  const removeUser = async (user: User) => {
    await fetch(`/users/${user.id}`, {
      method: "DELETE",
      headers: {
        "content-type": "application/json"
      }
    }).then(r => r.json());
    setUsers(users.filter(u => u.id !== user.id));
  };

  return (
    <userContext.Provider
      value={{
        state: { users, loading, selectedUser },
        actions: {
          fetchUsers,
          selectUser,
          addUser,
          removeUser
        }
      }}
    >
      {children}
    </userContext.Provider>
  );
};

export function useUserContext() {
  return useContext(userContext);
}
