import React, { useState } from "react";
import { ContactSider } from "./components/ContactSider";
import { ContactDetail } from "./components/ContactDetail";
import { useWindowSize, isOnMobile } from "../../hooks/useWindowSize";
import { useUserContext } from "../../context/UserContext";
import { AddUserModal } from "./components/AddUserModal";

export const DashboardPage: React.FC = () => {
  const windowSize = useWindowSize();
  const isMobile = isOnMobile(windowSize);
  const [addUserModalVisible, setAddUserModalVisible] = useState(false);
  const showAddUserModal = (val: boolean) => {
    return () => setAddUserModalVisible(val);
  };
  const {
    state: { selectedUser }
  } = useUserContext();
  const showSidebar = () =>
    (!selectedUser && isMobile) || !isMobile ? true : false;

  const showDetail = () =>
    (selectedUser && isMobile) || !isMobile ? true : false;

  return (
    <div style={{ display: "flex", height: "100%" }}>
      <AddUserModal
        visible={addUserModalVisible}
        hideModal={showAddUserModal(false)}
      />
      {showSidebar() && (
        <ContactSider showAddUserModal={showAddUserModal(true)} />
      )}
      {showDetail() && (
        <div
          style={{
            height: "100%",
            flex: "1",
            padding: isMobile ? "0px" : "25px"
          }}
        >
          <ContactDetail />
        </div>
      )}
    </div>
  );
};
