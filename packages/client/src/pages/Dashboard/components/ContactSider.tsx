import React, { useState, SyntheticEvent } from "react";
import { Input, Menu, Icon, Button } from "antd";
import { useUserContext } from "../../../context/UserContext";
import { ClickParam } from "antd/lib/menu";
import { useWindowSize, isOnMobile } from "../../../hooks/useWindowSize";

export const ContactSider: React.FC<{
  showAddUserModal: () => void;
}> = ({ showAddUserModal }) => {
  const {
    state: { users },
    actions: { selectUser }
  } = useUserContext();
  const [filter, setFilter] = useState("");
  const windowSize = useWindowSize();
  const isMobile = isOnMobile(windowSize);

  const handleFilterChange = (val: SyntheticEvent<HTMLInputElement>) => {
    setFilter(val.currentTarget.value);
  };

  const clickedMenuItem = (item: ClickParam) => {
    const user = users.find(u => {
      return u.id === item.key;
    });
    selectUser(user!);
  };

  return (
    <div
      style={{
        width: isMobile ? "100%" : "250px",
        height: "100%",
        display: "flex",
        flexDirection: "column"
      }}
    >
      <Input
        data-testid="filter-input"
        prefix={<Icon type="search" />}
        style={{ marginBottom: "10px" }}
        onChange={handleFilterChange}
      ></Input>
      <Menu
        style={{
          flex: "1",
          overflow: "scroll",
          marginBottom: "20px"
        }}
        onClick={clickedMenuItem}
      >
        {users
          .filter(u => {
            return filter.length > 0
              ? u.name.toLowerCase().includes(filter.toLowerCase())
              : true;
          })
          .map(user => {
            return (
              <Menu.Item data-testid="menu-item" key={user.id}>
                {user.name}
              </Menu.Item>
            );
          })}
      </Menu>
      <Button type="primary" icon="plus" onClick={showAddUserModal}>
        Add User
      </Button>
    </div>
  );
};
