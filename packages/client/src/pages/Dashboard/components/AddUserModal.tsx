import React from "react";
import { Modal } from "antd";
import { ModalProps } from "antd/lib/modal";
import { Formik } from "formik";
import { Form, Input } from "formik-antd";
import * as Yup from "yup";
import { useUserContext } from "../../../context/UserContext";

const AddUserSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  userName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  company: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  phone: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string()
    .email()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  website: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

export const AddUserModal: React.FC<{
  hideModal: () => void;
} & ModalProps> = ({ hideModal, ...props }) => {
  const {
    actions: { addUser }
  } = useUserContext();
  return (
    <Formik
      initialValues={{
        name: "",
        userName: "",
        company: "",
        phone: "",
        email: "",
        website: ""
      }}
      validationSchema={AddUserSchema}
      onSubmit={async values => {
        await addUser(values);
        hideModal();
      }}
    >
      {formik => (
        <Modal
          centered
          {...props}
          onOk={() => {
            formik.handleSubmit();
          }}
          onCancel={hideModal}
        >
          <Form onSubmit={formik.handleSubmit}>
            <Form.Item label="Name" name="name" htmlFor="name">
              <Input id="name" name="name" size="large" />
            </Form.Item>

            <Form.Item label="Username" name="userName" htmlFor="userName">
              <Input id="userName" name="userName" size="large" />
            </Form.Item>

            <Form.Item label="Company" name="company" htmlFor="company">
              <Input id="company" name="company" size="large" />
            </Form.Item>
            <Form.Item label="Email" name="email" htmlFor="email">
              <Input id="email" name="email" size="large" />
            </Form.Item>
            <Form.Item label="Phone" name="phone" htmlFor="phone">
              <Input id="phone" name="phone" size="large" />
            </Form.Item>
            <Form.Item label="Website" name="website" htmlFor="website">
              <Input id="website" name="website" size="large" />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};
