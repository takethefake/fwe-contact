import React from "react";
import { render, cleanup, fireEvent, act, wait } from "@testing-library/react";
import { ContactSider } from "./ContactSider";
import { userContext, initialState } from "../../../context/UserContext";

beforeEach(() => {
  cleanup();
});

describe("ContactSider", () => {
  it("should render all users inside menu", () => {
    const user1 = {
      id: "random-uuuid",
      name: "Daniel Schulz",
      userName: "dschulz",
      phone: "0123456789",
      email: "daniel.schulz@incloud.de",
      website: "incloud.de",
      company: "Incloud"
    };
    const user2 = {
      id: "random-uuuid-2",
      name: "Thomas Sauer",
      userName: "tsauer",
      phone: "01234567890",
      email: "thomas.sauer@bevelop.de",
      website: "bevelop.de",
      company: "h_da"
    };
    const userArray = [user1, user2];
    const { getAllByTestId } = render(
      <userContext.Provider
        value={{
          ...initialState,
          state: { ...initialState.state, users: userArray }
        }}
      >
        <ContactSider showAddUserModal={jest.fn()} />
      </userContext.Provider>
    );
    const menuItems = getAllByTestId("menu-item");
    expect(menuItems.length).toBe(2);
    menuItems.map((item, idx) => {
      expect(item.innerHTML).toBe(userArray[idx].name);
    });
  });

  it("should filter based on name", async () => {
    const user1 = {
      id: "random-uuuid",
      name: "Daniel Schulz",
      userName: "dschulz",
      phone: "0123456789",
      email: "daniel.schulz@incloud.de",
      website: "incloud.de",
      company: "Incloud"
    };
    const user2 = {
      id: "random-uuuid-2",
      name: "Thomas Sauer",
      userName: "tsauer",
      phone: "01234567890",
      email: "thomas.sauer@bevelop.de",
      website: "bevelop.de",
      company: "h_da"
    };
    const userArray = [user1, user2];
    const { getByTestId, getAllByTestId } = render(
      <userContext.Provider
        value={{
          ...initialState,
          state: { ...initialState.state, users: userArray }
        }}
      >
        <ContactSider showAddUserModal={jest.fn()} />
      </userContext.Provider>
    );
    await act(async () => {
      const filterInput = getByTestId("filter-input");
      fireEvent.change(filterInput, { target: { value: "Daniel" } });
    });
    wait(() => {
      const menuItems = getAllByTestId("menu-item");
      expect(menuItems.length).toBe(1);
      expect(menuItems[0].innerHTML).toBe(user1.name);
    });
  });

  it("should select user on click", async () => {
    const user1 = {
      id: "random-uuuid",
      name: "Daniel Schulz",
      userName: "dschulz",
      phone: "0123456789",
      email: "daniel.schulz@incloud.de",
      website: "incloud.de",
      company: "Incloud"
    };
    const selectUserMock = jest.fn();
    const { getByTestId, getAllByTestId } = render(
      <userContext.Provider
        value={{
          actions: { ...initialState.actions, selectUser: selectUserMock },
          state: { ...initialState.state, users: [user1] }
        }}
      >
        <ContactSider showAddUserModal={jest.fn()} />
      </userContext.Provider>
    );
    const menuItem = getByTestId("menu-item");
    fireEvent.click(menuItem);
    expect(selectUserMock).toHaveBeenCalledTimes(1);
    expect(selectUserMock).toHaveBeenCalledWith(user1);
  });

  it("should open modal when the user wants to create a new user", () => {
    const showAddUserModalMock = jest.fn();
    const { getByText } = render(
      <userContext.Provider
        value={{
          ...initialState
        }}
      >
        <ContactSider showAddUserModal={showAddUserModalMock} />
      </userContext.Provider>
    );
    const createUserButton = getByText(/add user/i);
    fireEvent.click(createUserButton);
    expect(showAddUserModalMock).toHaveBeenCalledTimes(1);
  });
});
