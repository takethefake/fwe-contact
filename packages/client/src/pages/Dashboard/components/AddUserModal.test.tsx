import React from "react";
import { AddUserModal } from "./AddUserModal";
import { render, cleanup, fireEvent, act, wait } from "@testing-library/react";
import { userContext, initialState } from "../../../context/UserContext";

beforeEach(() => {
  cleanup();
});

describe("AddUserModal", () => {
  it("should have all necessary fields", () => {
    const { getByLabelText } = render(
      <AddUserModal hideModal={jest.fn()} visible={true} />
    );
    const name = getByLabelText(/^name/i);
    expect(name).toBeInTheDocument();
    const username = getByLabelText(/username/i);
    expect(username).toBeInTheDocument();
    const company = getByLabelText(/company/i);
    expect(company).toBeInTheDocument();
    const phone = getByLabelText(/phone/i);
    expect(phone).toBeInTheDocument();
    const email = getByLabelText(/email/i);
    expect(email).toBeInTheDocument();
    const website = getByLabelText(/website/i);
    expect(website).toBeInTheDocument();
  });

  it("should send data to context when submitted", async () => {
    const addUserMock = jest.fn();
    const { getByLabelText, getByText } = render(
      <userContext.Provider
        value={{
          ...initialState,
          actions: { ...initialState.actions, addUser: addUserMock }
        }}
      >
        <AddUserModal hideModal={jest.fn()} visible={true} />
      </userContext.Provider>
    );
    const testuser = {
      name: "Daniel Schulz",
      userName: "dschulz",
      company: "Incloud",
      phone: "0123456789",
      email: "daniel.schulz@incloud.de",
      website: "incloud.de"
    };

    const name = getByLabelText(/^name/i);
    const username = getByLabelText(/username/i);
    const company = getByLabelText(/company/i);
    const phone = getByLabelText(/phone/i);
    const email = getByLabelText(/email/i);
    const website = getByLabelText(/website/i);
    const submit = getByText(/ok/i);
    await act(async () => {
      fireEvent.change(name, { target: { value: testuser.name } });
      fireEvent.change(username, { target: { value: testuser.userName } });
      fireEvent.change(company, { target: { value: testuser.company } });
      fireEvent.change(phone, { target: { value: testuser.phone } });
      fireEvent.change(email, { target: { value: testuser.email } });
      fireEvent.change(website, { target: { value: testuser.website } });
      fireEvent.click(submit);
    });
    wait(() => {
      expect(addUserMock).toHaveBeenCalledTimes(1);
      expect(addUserMock).toHaveBeenCalledWith(testuser);
    });
  });

  it("should not send data when form is empty ", async () => {
    const addUserMock = jest.fn();
    const { getByLabelText, getByText } = render(
      <userContext.Provider
        value={{
          ...initialState,
          actions: { ...initialState.actions, addUser: addUserMock }
        }}
      >
        <AddUserModal hideModal={jest.fn()} visible={true} />
      </userContext.Provider>
    );
    const submit = getByText(/ok/i);
    await act(async () => {
      fireEvent.click(submit);
    });
    wait(() => {
      expect(addUserMock).toHaveBeenCalledTimes(0);
    });
  });
});
