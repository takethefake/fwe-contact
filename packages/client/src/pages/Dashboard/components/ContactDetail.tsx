import React from "react";
import { useUserContext } from "../../../context/UserContext";
import {
  Empty,
  Avatar,
  Descriptions,
  Icon,
  Button,
  PageHeader,
  Modal
} from "antd";
import { isOnMobile, useWindowSize } from "../../../hooks/useWindowSize";

const randHexColor = () => {
  const colors = [
    Math.round(Math.random() * 255),
    Math.round(Math.random() * 255),
    Math.round(Math.random() * 255)
  ];
  const hex = colors.reduce((prev, cur) => {
    prev += cur.toString(16);
    return prev;
  }, "#");
  return hex;
};

export const ContactDetail: React.FC = () => {
  const windowSize = useWindowSize();
  const isMobile = isOnMobile(windowSize);
  const {
    state: { selectedUser },
    actions: { selectUser, removeUser }
  } = useUserContext();
  if (!selectedUser) {
    return (
      <div
        style={{
          height: "100%",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Empty description="No user selected" />
      </div>
    );
  }
  return (
    <div>
      {isMobile && (
        <PageHeader
          title="Back"
          onBack={() => {
            selectUser(null);
          }}
        />
      )}
      <div
        style={{ display: "flex", alignItems: "center", padding: "16px 24px" }}
      >
        <Avatar
          style={{ backgroundColor: randHexColor(), verticalAlign: "middle" }}
          size={64}
        >
          {selectedUser.name.substr(0, 1).toUpperCase()}
        </Avatar>
        <div
          style={{
            flex: "1",
            display: "flex",
            flexDirection: "column",
            marginLeft: "20px"
          }}
        >
          <h1>{selectedUser.name}</h1>
          <span>{selectedUser.company}</span>
        </div>
      </div>
      <div style={{ padding: "25px 0px 25px 108px" }}>
        <Descriptions title="User Info" column={{ xs: 1, sm: 2 }}>
          <Descriptions.Item
            label={
              <>
                <Icon type="user" /> Username
              </>
            }
          >
            {selectedUser.userName}
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <>
                <Icon type="mail" /> Mail
              </>
            }
          >
            <a href={`mailto:${selectedUser.email}`}>{selectedUser.email}</a>
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <>
                <Icon type="phone" /> Phone
              </>
            }
          >
            <a href={`tel:${selectedUser.phone}`}>{selectedUser.phone}</a>
          </Descriptions.Item>
          <Descriptions.Item
            label={
              <>
                <Icon type="home" /> Website
              </>
            }
          >
            <a
              href={selectedUser.website}
              target="_blank"
              rel="noopener noreferrer"
            >
              {selectedUser.website}
            </a>
          </Descriptions.Item>
        </Descriptions>
        <Button
          type="danger"
          onClick={() => {
            Modal.confirm({
              title: "Confirm",
              content: "Do you really want to delete the user?",
              okText: "Delete",
              onOk: async () => {
                await removeUser(selectedUser);
                selectUser(null);
              },
              cancelText: "Cancel"
            });
          }}
        >
          Remove
        </Button>
      </div>
    </div>
  );
};
