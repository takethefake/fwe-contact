import { createConnection, getConnectionOptions } from "typeorm";

export const createDatabaseConnection = async () => {
  let retries = 5;
  while (retries) {
    try {
      const config = await getConnectionOptions("default");
      return createConnection(config);
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.log(err);
      retries -= 1;
      // tslint:disable-next-line:no-console
      console.log(`[DB-Connection] Retries left: ${retries}`);
      // wait 5 seconds
      await new Promise(res => setTimeout(res, 5000));
    }
  }

  throw new Error("Could not establish a database connection!");
};
