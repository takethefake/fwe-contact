import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn
} from "typeorm";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: 255, nullable: false, unique: true })
  email: string;

  @Column("varchar", { length: 255, nullable: false })
  website: string;

  @Column("varchar", { length: 255, nullable: false })
  company: string;

  @Column("varchar", { length: 255, nullable: false })
  phone: string;

  @Column("varchar", { length: 255, nullable: false })
  name: string;

  @Column("varchar", { length: 255, nullable: false })
  userName: string;

  @BeforeInsert()
  @BeforeUpdate()
  canonicalizeEmail() {
    this.email = this.email.toLowerCase();
  }
}
