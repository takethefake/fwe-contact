import { createDatabaseConnection } from "./util/createDatabaseConnection";
import express = require("express");
import bodyParser = require("body-parser");
import { Connection } from "typeorm";
import { User } from "./entity/User";

const startServer = async () => {
  const con = (await createDatabaseConnection()) as Connection;
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.get("/users", async (_, res) => {
    try {
      const users = await con.getRepository(User).find();
      res.send(users);
    } catch (e) {
      console.log(e);
    }
  });

  app.post("/users", async (req, res) => {
    try {
      const user = User.create({ ...req.body });
      const savedUser = await user.save();
      res.send(savedUser);
    } catch (e) {
      res.send(e);
    }
  });

  app.delete("/users/:id", async (req, res) => {
    try {
      const user = await con.getRepository(User).findOneOrFail(req.params.id);
      await user.remove();

      res.send({ id: req.params.id });
    } catch (e) {
      res.send(e);
    }
  });

  app.listen(4000, () => {
    console.log("runs on 4000");
  });
};

startServer();
