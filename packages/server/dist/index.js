"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const createDatabaseConnection_1 = require("./util/createDatabaseConnection");
const express = require("express");
const bodyParser = require("body-parser");
const User_1 = require("./entity/User");
const startServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const con = (yield createDatabaseConnection_1.createDatabaseConnection());
    const app = express();
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.get("/users", (_, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const users = yield con.getRepository(User_1.User).find();
            res.send(users);
        }
        catch (e) {
            console.log(e);
        }
    }));
    app.post("/users", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const user = User_1.User.create(Object.assign({}, req.body));
            const savedUser = yield user.save();
            res.send(savedUser);
        }
        catch (e) {
            res.send(e);
        }
    }));
    app.delete("/users/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const user = yield con.getRepository(User_1.User).findOneOrFail(req.params.id);
            yield user.remove();
            res.send({ id: req.params.id });
        }
        catch (e) {
            res.send(e);
        }
    }));
    app.listen(4000, () => {
        console.log("runs on 4000");
    });
});
startServer();
//# sourceMappingURL=index.js.map