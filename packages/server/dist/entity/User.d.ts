import { BaseEntity } from "typeorm";
export declare class User extends BaseEntity {
    id: string;
    email: string;
    website: string;
    company: string;
    phone: string;
    name: string;
    userName: string;
    canonicalizeEmail(): void;
}
