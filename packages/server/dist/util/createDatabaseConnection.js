"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
exports.createDatabaseConnection = () => __awaiter(void 0, void 0, void 0, function* () {
    let retries = 5;
    while (retries) {
        try {
            const config = yield typeorm_1.getConnectionOptions("default");
            return typeorm_1.createConnection(config);
        }
        catch (err) {
            console.log(err);
            retries -= 1;
            console.log(`[DB-Connection] Retries left: ${retries}`);
            yield new Promise(res => setTimeout(res, 5000));
        }
    }
    throw new Error("Could not establish a database connection!");
});
//# sourceMappingURL=createDatabaseConnection.js.map